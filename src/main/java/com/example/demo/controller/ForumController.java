package com.example.demo.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面(絞り込み機能あり)
	@GetMapping
	public ModelAndView searchReport(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();

		String start = request.getParameter("start_date");
		String end = request.getParameter("end_date");

		List<Report> contentDate = reportService.searchReport(start, end);
		List<Comment> commentDate = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 取得データをセット
		mav.addObject("start_date", start);
		mav.addObject("end_date", end);
		mav.addObject("contents", contentDate);
		mav.addObject("comments", commentDate);
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		Date date = new Date();
		report.setCreated_date(date);
		report.setUpdated_date(date);
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//投稿削除
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}

	// 編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Report report = reportService.editReport(id);
		mav.addObject("formModel", report);
		mav.setViewName("/edit");
		return mav;
	}

	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		report.setId(id); // UrlParameterのidを更新するentityにセット
		Report reportDate = reportService.editReport(id);
		report.setCreated_date(reportDate.getCreated_date());
		report.setUpdated_date(reportDate.getUpdated_date());
		reportService.saveReport(report); // 編集した投稿を更新
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	//返信処理
	@PostMapping("/addComment/{report_id}")
	public ModelAndView addComment(@PathVariable Integer report_id, @ModelAttribute("formModel") Comment comment) {
		comment.setReportId(report_id);
		commentService.saveComment(comment); //編集をテーブルに格納
		return new ModelAndView("redirect:/");//rootへリダイレクト
	}

	//返信編集画面
	@GetMapping("/commentEdit/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Comment comment = commentService.editComment(id);
		comment.setCreated_date(comment.getCreated_date());
		comment.setUpdated_date(comment.getUpdated_date());
		mav.addObject("formModel", comment);
		mav.setViewName("/commentEdit");
		return mav;
	}

	//返信編集処理
	@PutMapping("/updateComment/{id}")
	public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {
		Comment getCommentId = commentService.editComment(id);
		comment.setReportId(getCommentId.getReportId());
		commentService.updateComment(comment);
		return new ModelAndView("redirect:/");
	}

	//返信削除
	@DeleteMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}

}
