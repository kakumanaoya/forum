package com.example.demo.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;


@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {
	@Query("select t from Report t where t.created_date between :start_date and :end_date order by updated_date desc")
	List<Report> findByReport(@Param("start_date") Timestamp start_date,@Param("end_date")Timestamp end_date);

	@Query("select t from Report t order by created_date asc")
	List<Report> findAllReport();
}