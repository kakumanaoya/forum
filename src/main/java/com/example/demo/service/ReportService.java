package com.example.demo.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport() {
		return reportRepository.findAll();
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	//レコードの削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}
	//レコード1件の取得
	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	//レコード絞り込み
	public List<Report> searchReport(String start_date, String end_date) {
		List<Report> report = null;
		if ((StringUtils.hasLength(start_date)) & (StringUtils.hasText(start_date))) {
        	start_date += " 00:00:00";
        } else {
        	String defaultDate = "2020-01-01 00:00:00";
        	start_date = defaultDate;
        }

        if ((StringUtils.hasLength(end_date)) & (StringUtils.hasText(end_date))) {
        	end_date += " 23:59:59";
        } else {

            Date date = new Date();
    		SimpleDateFormat format = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
    		String nowDate = format.format(date);
        	end_date = nowDate;
        }
        Timestamp strDate = Timestamp.valueOf(start_date);
    	Timestamp endDate = Timestamp.valueOf(end_date);
        report = reportRepository.findByReport(strDate, endDate);
		return report;
	}

}



